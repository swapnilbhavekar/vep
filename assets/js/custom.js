// var sitescript = {
//     init: function() {
//         this.homePage();
//     },
//     homePage: function() {
//         return console.log("d");
//     }
// };




// //isotop j query start
// $('.grid').isotope({
//     // options
//     itemSelector: '.grid-item',
//     layoutMode: 'fitRows'
// });

// var $grid = $('.grid').isotope({
//     // main isotope options
//     itemSelector: '.grid-item',
//     // set layoutMode
//     layoutMode: 'cellsByRow',
//     // options for cellsByRow layout mode
//     cellsByRow: {
//         columnWidth: 200,
//         rowHeight: 150
//     },
//     // options for masonry layout mode
//     masonry: {
//         columnWidth: '.grid-sizer'
//     }
// })



//isotop j query end

//masonary Js Start

$('.grid').masonry({
    // options
    itemSelector: '.grid-item',

});



function masnry() {

    var $container = $('.grid');
    // $container.imagesLoaded(function() {
    //     $container.masonry({
    //         columnWidth: '.grid-item',
    //         itemSelector: '.grid-item'
    //     });
    // });

    //Reinitialize masonry inside each panel after the relative tab link is clicked - 
    $('a[data-toggle=tab]').each(function() {
        var $this = $(this);
        $this.on('shown.bs.tab', function() {
            $container.imagesLoaded(function() {
                $container.masonry({
                    columnWidth: '.grid-item',
                    itemSelector: '.grid-item'
                });
            });
        }); //end shown
    }); //end each

};





//masonary JS End






function swiper() {

    //  slider js
    var swiper = new Swiper('.swiper-container', {
        pagination: {
            el: '.swiper-pagination',
            dynamicBullets: true,
        },
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var paneTarget = $(e.target).attr('href');
        var $thePane = $('.tab-pane' + paneTarget);
        var paneIndex = $thePane.index();
        if ($thePane.find('.swiper-container').length > 0 && 0 === $thePane.find('.swiper-slide-active').length) {
            swiper[paneIndex].update();
        }
    });
}






//video tabs end


var width = $(window).width();

var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

function auto_play() {
    if (!isChrome) {
        $('#audio').remove();
    } else {
        $('#player').remove();
    }
}

var play_state = true;
var isSafari = /constructor/i.test(window.HTMLElement) || (function(p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
if (isSafari) {
    $('.audio_button').toggleClass('icon-volume-up icon-volume-off');
    play_state = false;
}
var x = document.getElementById("player");

function magnigicPopup() {
    // $('.popup-gallery').magnificPopup({
    //     delegate: 'a', // the selector for gallery item
    //     type: 'image',
    //     gallery: {
    //         enabled: true
    //     }
    // });
}

function toggle_music() {
    $('.audio_button').click(function() {
        if (play_state == true) {
            play_state = false;
            if (isChrome) {
                $('#audio').remove();
            } else {
                x.pause();
            }
            play_state = false;
        } else {
            if (isChrome) {
                $('body').prepend('<iframe src="assets/audio/tumbi.mp3" allow="autoplay" id="audio" style="display:none"></iframe>');
            } else {
                x.play();
            }
            play_state = true;
        }
        $(this).toggleClass('icon-volume-up icon-volume-off');
    });
}


function animation() {
    if (width > 992) {
        skrollr.init({
            smoothScrolling: false,
            //mobileDeceleration: 0.004
        });
    } else {
        skrollr.init().destroy(); // destroy animation if over 767
    }
}
$.validator.addMethod("valueNotEquals", function(value, element, arg) {
    return arg !== value;
}, "Value must not equal arg.");

function validate_registration() {
    $(".appointment-form").validate({
        rules: {
            name: {
                required: true
            },
            number: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            noofpeople: {
                valueNotEquals: "default"
            }
        },
        messages: {
            noofpeople: { valueNotEquals: "Please select no. of people!" }
        },
        errorPlacement: function(error, element) {
            if (element.is("select")) {
                error.insertAfter(element.next());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            $('[name=noofpeople]').find(":selected").text();
            if ($('[name=noofpeople]').find(":selected").val() > 1) {
                $('#registration-popup').modal('show');
            }
            // $('.volunteer-btn').css('display', 'none');
            // $('.loader').css('display', 'block');
            // $('.contact_error').html('Please wait your message is sending..').css('display', 'block');

            // $.ajax({
            //     type: 'POST',
            //     data: $('.appointment-form').serialize(),
            //     url: 'sendmail.php',
            //     success: function(result) {

            //         if (result) {
            //             $('.form-control').val('');
            //             $('.contact_error').html('Thank you for your comments..').addClass('text-success').removeClass('text-danger');
            //         } else {
            //             $('.contact_error').html('Please Try again..').addClass('text-danger').removeClass('text-success');
            //         }
            //         $('.contact-btn').css('display', 'inline-block');
            //         $('.loader').css('display', 'none');
            //     }
            // });
            return false;
        }
    });
}

function join_singing() {
    $(".join-singing").validate({
        rules: {
            name: {
                required: true
            },
            tel: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            file: {
                required: true,
            },

        },
        messages: {
            userType: { valueNotEquals: "Please select proper user type" }
        },
        errorPlacement: function(error, element) {

            if (element.hasClass("input-file") == true) {
                error.appendTo(element.closest(".form-group"));
            } else {
                error.insertAfter(element);
            }

        },
        submitHandler: function(form) {
            $('.join-singing .signing-btn').css('display', 'none');
            $('.join-singing .loader').css('display', 'inline');
            $('.join-singing .contact-error').show().html('Please wait your message is sending..');

            form.submit();

            /*$.ajax({
                type: 'POST',
                data: $('.join-singing').serialize(),
                url: 'sendAttachment.php',
                success: function(result) {
                    if (result) {
                        $('.join-singing')[0].reset();
                        $('.join-singing .contact-error').html('Thank you for your comments..').addClass('text-success').removeClass('text-danger');
                        $('.join-singing .loader').css('display', 'none');
                        $('.join-singing .bs-btn').css('display', 'inline-block');

                    } else {
                        $('.join-singing .contact-error').html('Please Try again..').addClass('text-danger').removeClass('text-success');
                    }
                }

            });
            return false;*/
        }
    });
}

function volunteer_form() {
    $(".volunteer-form").validate({
        rules: {
            name: {
                required: true
            },
            number: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            userType: {
                //valueNotEquals: "default"
                required: true
            },
            regionDtls: {
                required: true,
            },
        },
        // messages: {
        //     userType: { valueNotEquals: "Please select proper user type" }
        // },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            // if (element.is("select")) {
            //     error.insertAfter(element.next());
            // } else {
            //     error.insertAfter(element);
            // }
        },
        submitHandler: function(form) {
            $('.volunteer-form .volunteer-btn').css('display', 'none');
            $('.volunteer-form .loader').css('display', 'inline');
            // $('.volunteer-form .contact-error').show();
            $('.volunteer-form .contact-error').show().html('Please wait your message is sending..');

            $.ajax({
                type: 'POST',
                data: $('.volunteer-form').serialize(),
                url: 'sendmail.php',
                success: function(result) {
                    if (result) {
                        $('.volunteer-form')[0].reset();
                        $('.volunteer-form .contact-error').html('Thank you for your comments..').addClass('text-success').removeClass('text-danger');
                        $('.volunteer-form .loader').css('display', 'none');
                        $('.volunteer-form .bs-btn').css('display', 'inline-block');

                    } else {
                        $('.volunteer-form .contact-error').html('Please Try again..').addClass('text-danger').removeClass('text-success');
                    }
                }

            });
            return false;
        }
    });
}

function query_form() {
    $(".query-form").validate({
        rules: {
            name: {
                required: true
            },
            number: {
                required: true,
                number: true
            },
            message: {
                required: true,
            },
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        submitHandler: function(form) {
            $('.query-form .query-btn').css('display', 'none');
            $('.query-form .loader').css('display', 'inline');
            $('.query-form .contact-error').show().html('Please wait your message is sending..');

            $.ajax({
                type: 'POST',
                data: $('.query-form').serialize(),
                url: 'sendQuery.php',
                success: function(result) {
                    if (result) {
                        $('.query-form')[0].reset();
                        $('.query-form .contact-error').html('Thank you for your comments..').addClass('text-success').removeClass('text-danger');
                        $('.query-form .loader').css('display', 'none');
                        $('.query-form .query-btn').css('display', 'inline-block');
                        setTimeout(function() {
                            $('.query-form .contact-error').hide();
                        }, 2000);
                    } else {
                        $('.query-form .contact-error').html('Please Try again..').addClass('text-danger').removeClass('text-success');
                    }
                }

            });
            return false;
        }
    });
}

function singing_form() {
    $(".query-form").validate({
        rules: {
            name: {
                required: true
            },
            number: {
                required: true,
                number: true
            },
            message: {
                required: true,
            },
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        submitHandler: function(form) {
            $('.query-form .query-btn').css('display', 'none');
            $('.query-form .loader').css('display', 'inline');
            $('.query-form .contact-error').show().html('Please wait your message is sending..');

            $.ajax({
                type: 'POST',
                data: $('.query-form').serialize(),
                url: 'sendQuery.php',
                success: function(result) {
                    if (result) {
                        $('.query-form')[0].reset();
                        $('.query-form .contact-error').html('Thank you for your comments..').addClass('text-success').removeClass('text-danger');
                        $('.query-form .loader').css('display', 'none');
                        $('.query-form .query-btn').css('display', 'inline-block');

                    } else {
                        $('.query-form .contact-error').html('Please Try again..').addClass('text-danger').removeClass('text-success');
                    }
                }

            });
            return false;
        }
    });
}

function menuClicking() {
    $(".mod-hr-tabs>div .nav li a").click(function(e) {
        e.preventDefault();
        $(".mod-hr-tabs>div .nav li").removeClass("active");
        $(this).parents("li").addClass("active");
        var data = $(this).data("href");
        $("#attachment-type").attr("value", data);
        console.log(data);
        if (data == "Singing") {
            $("#Singing").show();
            $("#Poetry,#Other").hide();

        } else if (data == "Poetry") {
            $("#Poetry").show();
            $("#Singing,#Other").hide();
        } else {
            $("#Other").show();
            $("#Singing,#Poetry").hide();
        }
    });
}


function modalPopup() {
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
            }
        }
    });
}

$(function() {
    auto_play();
    toggle_music();
    validate_registration();
    volunteer_form();
    join_singing();
    //query_form();
    singing_form();
    //sitescript.init();
    new WOW().init();
    animation();
    // magnigicPopup();
    modalPopup();
    swiper();
    
    //$(window).paroller();

    //$("[data-paroller-factor]").paroller();

    // $('button').on('click', function() {
    //     $('body').toggleClass('open');
    // });


    $(".input-bx").click(function() {
        $(this).next(".input-file").trigger("click");
    });

    $('a.delete-row').on('click', function(e) {
        e.preventDefault();
        $(this).closest("tr").remove();
    });

    $('a.add-row').on('click', function(e) {
        e.preventDefault();
        var parentRw = $(this).closest("tr");
        var newR = $(parentRw).clone(true);
        newR.find("input[type=text], input[type=email]").val("");
        newR.insertAfter(parentRw);
    });

    $('a.edit-row').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass("edit-row icon-check-square-o");

        if ($(this).hasClass('icon-check-square-o')) {
            var parentRw = $(this).closest("tr");
            $(parentRw).find(".form-control").prop('readonly', false);
        } else {
            var parentRw = $(this).closest("tr");
            $(parentRw).find(".form-control").prop('readonly', true);
        }
    });

    $('a.goto-bottom').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 1000, 'linear');
    });

    $('.map-link').on('click', function(e) {
        e.preventDefault();
        $(".contact-box").fadeOut();
        $(".map-box").fadeIn();
    });

    $('.show-contact').on('click', function(e) {
        e.preventDefault();
        $(".map-box").fadeOut();
        $(".contact-box").fadeIn();
    });

    // $('.mod-vr-tabs .nav li a').on('shown.bs.tab', function(e) {
    //     // console.log(e.target); // activated tab
    //     knowMore(".know-more");
    // });

    knowMore(".know-more");

    /*For top nav animation*/
    $('.collapse-button').click(function() {
        $('body').toggleClass('open');
        if ($('body').hasClass('open')) {
            $(this).addClass('exp');
        } else {
            $(this).removeClass('exp');
        }

    });

    $(".bs-header ul.menu li, .register-btn, .donation-btn, .mod-bx .link, .coming-soon").click(function() {
        $("body").removeClass("open");
        $(".collapse-button").removeClass("exp");
        var element = $(this).data("id");
        $('html, body').animate({
            scrollTop: $(element).offset().top
        }, 2000);
    });

    $("#audio").click(function() {
        $(this).toggleClass("icon-volume-up icon-volume-off");
    });

    $('.queryName').click(function() {
        $(this).parent('.querySec').toggleClass('show');
        if ($('.querySec').hasClass('show')) {
            $(this).removeClass('show');
        } else {
            $(this).addClass('show');
        }
    });

    drpdwn();

    $('.mod-register.mod-vlntr-bx .nav-pills>li>a').on('shown.bs.tab', function(e) {
        $('.bs-drpdwn').trigger('render');
    });

    $(".bs-drpdwn").on('change', function() {
        if ($(this).val() == "2") {
            $("#reg-btn").text("Next");
        } else {
            $("#reg-btn").text("Send");
        }
    });

    timer("March 29, 2019 00:00:00")

    var showflag = 1;
    $("#add_form").validate({
        rules: {
            name: {
                required: true,
            },
            contact_no: {
                required: true,
            },
            email_id: {
                required: true,
            },
            no_of_people: {
                valueNotEquals: "default"
            },
        },
        messages: {
            no_of_people: { valueNotEquals: "Please select no. of people!" }
        },
        errorPlacement: function(error, element) {
            if (element.is("select")) {
                error.insertAfter(element.next());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            if ($('[name=no_of_people]').find(":selected").val() > 1) {
                if (showflag == 1) {
                    $('input.u1').val($('#name').val());
                    $('input.u2').val($('#contact_no').val());
                    $('input.u3').val($('#email_id').val());

                    $('#registration-popup').modal('show');
                    return false;
                }

                if (showflag == 0) {
                    var isSubmitFlag = 0;
                    var isDuplicateFlag = 0;
                    var uunameArr = [];
                    var uuemailArr = [];
                    var uumobileArr = [];
                    $(".moreUserClass .data-row").each(function() {
                        //if($(this).find('.uuname').val() == '' || $(this).find('.uuemail').val() == '' || $(this).find('.uumobile').val() == ''){
                        if ($(this).find('.uuname').val() == '') {
                            isSubmitFlag = 1;
                            return false;
                        }

                        if ($(this).find('.uuname').val() != '') {
                            if (typeof uunameArr !== 'undefined' && uunameArr.length > 0) {
                                if (uunameArr.indexOf($(this).find('.uuname').val()) > -1) {
                                    isDuplicateFlag = 1;
                                    return false;
                                } else {
                                    uunameArr.push($(this).find('.uuname').val());
                                }
                            } else {
                                uunameArr.push($(this).find('.uuname').val());
                            }
                        }

                        /*if($(this).find('.uuemail').val() != ''){
                            if (typeof uuemailArr !== 'undefined' && uuemailArr.length > 0) {
                                if(uuemailArr.indexOf($(this).find('.uuemail').val()) > -1){
                                    isDuplicateFlag = 1;                            
                                    return false;
                                }else{
                                    uuemailArr.push($(this).find('.uuemail').val());
                                }
                            }else {
                                uuemailArr.push($(this).find('.uuemail').val());
                            }
                        }

                        if($(this).find('.uumobile').val() != ''){
                            if (typeof uumobileArr !== 'undefined' && uumobileArr.length > 0) {
                                if(uumobileArr.indexOf($(this).find('.uumobile').val()) > -1){
                                    isDuplicateFlag = 1;                            
                                    return false;
                                }else{
                                    uumobileArr.push($(this).find('.uumobile').val());
                                }
                            }else {
                                uumobileArr.push($(this).find('.uumobile').val());
                            }
                        }*/
                    });
                }
            }

            if (isSubmitFlag == 1) {
                alert("Please enter all user details to continue.");
                return false;
            }

            if (isDuplicateFlag == 1) {
                alert("No duplicate Name are allowed.");
                return false;
            }

            $("#reg-btn").hide();
            $("#loading").show();
            //var form = $('#add_form')[0];
            //var formData = new FormData(form);

            if ($('[name=no_of_people]').find(":selected").val() > 1) {
                var uname = $('input[name="uname[]"]').map(function() {
                    return this.value;
                }).get();

                var umobile = $('input[name="umobile[]"]').map(function() {
                    return this.value;
                }).get();

                var uemail = $('input[name="uemail[]"]').map(function() {
                    return this.value;
                }).get();

                var postData = {
                    "name": $('#name').val(),
                    "contact_no": $('#contact_no').val(),
                    "email_id": $('#email_id').val(),
                    "no_of_people": $('#no_of_people').val(),
                    "uname": uname,
                    "uemail": umobile,
                    "umobile": uemail,
                };
            } else {
                var postData = {
                    "name": $('#name').val(),
                    "contact_no": $('#contact_no').val(),
                    "email_id": $('#email_id').val(),
                    "no_of_people": $('#no_of_people').val(),
                };
            }

            $.ajax({
                url: SITEURL + "api/Users/add",
                type: "post",
                cache: false,
                dataType: "json",
                data: postData,
                //processData: false,
                //contentType: false,
                success: function(response) {
                    if (response["responseCode"] == "success") {
                        $("#add_form").find("input[type=text], input[type=file], input[type=email], textarea").val("");
                        $('select option[value="default"]').attr("selected", true);
                        $(".more-users").find("input[type=text]").val("");
                        $("#reg-btn").show();
                        $('#registration-popup').modal('hide');
                        $("#loading").hide();
                        alert("Registeration done successfully");
                        setInterval(function() {
                            location.reload();
                        }, 2000);

                    } else if (response["responseCode"] == "error") {
                        $("#reg-btn").show();
                        $("#loading").hide();
                        alert(response["responseMessage"]);
                    } else {
                        $("#reg-btn").show();
                        $("#loading").hide();
                        alert("Something went wrong, Please try again.");
                    }
                }
            });
        }
    });

    $('#btnMoreUser').click(function() {
        showflag = 0;
        $("#reg-btn").trigger("click");
        //$('#add_form').submit();
    });


    $("#upload_form").validate({
        rules: { upload_file: { required: true, } },
        submitHandler: function(form) {
            $("#submitBtnUpload").hide();
            $("#loadingFileUpload").show();
            form.submit();
        }
    });

});


function drpdwn() {
    $('.bs-drpdwn').customSelect();
    $('.select-parent select').each(function() {
        $(this).css('z-index', '999');
    });
}

function knowMore(btn) {
    $(document).off('click', btn).on('click', btn, function(e) {
        e.preventDefault();
        var elementBx = $(this).closest(".row").siblings(".timeline");
        var parentBx = $(this).closest(".row").siblings(".timeline").closest(".tab-pane");
        console.log(elementBx.hasClass('less') + "Data");
        if (elementBx.hasClass('less')) {
            elementBx.removeClass('less');
            parentBx.find(".know-more").text("Know Less");

        } else {
            elementBx.addClass('less');
            parentBx.find(".know-more").text("Know More");
            $('html, body').animate({ scrollTop: $("#event-schedule").offset().top }, 1000, 'linear');
        }
        //$(this).closest(".row").siblings(".timeline").toggleClass("less");
        // setTimeout(function() {
        //     $('html, body').animate({ scrollTop: $("#event-schedule").offset().top }, 1000, 'linear');
        // }, 500);

        // var parentBox = $(this).parents(".tab-pane"),
        //     boxHeight = $(parentBox).find(".timeline-bx").eq(0).outerHeight(),
        //     totalHeight = $(parentBox).find(".timeline-bx").length * boxHeight,
        //     currentHeight = $(parentBox).find(".timeline").outerHeight() + boxHeight;
        // console.log($(parentBox).attr("id"));
        // if (totalHeight > currentHeight) {
        //     $(parentBox).find(".timeline").css("height", totalHeight);
        //     $(this).hide();
        //     //console.log(totalHeight + " > " + currentHeight);
        // }
    });
}

function getFile() {
    var thefile = document.getElementById('poetry-doc');

    var fp = thefile;
    var lg = fp
        .files.length; // get length
    var items = fp.files;
    var fragment = "";
    if (lg > 0) {
        for (var i = 0; i < lg; i++) {
            var fileName = items[i].name; // get file name
            var fileSize = items[i].size; // get file size 
            var fileType = items[i].type; // get file type
            // append li to UL tag to display File info
            fragment += fileName + " " + fileSize + " bytes. Type :" + fileType;
        }
        //console.log(fragment);
        $(".input-bx, .input-file").hide();
        $(".file-name").html(fileName);
    }
}

function timer(timerData) {
    // Set the date we're counting down to
    var countDownDate = new Date(timerData).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        //document.getElementById("demo").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
        //console.log(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");

        $(".days").html(days);
        $(".hours").html(hours);
        $(".minutes").html(minutes);
        $(".seconds").html(seconds);

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            //document.getElementById("demo").innerHTML = "EXPIRED";
            $(".days").html(0);
            $(".hours").html(0);
            $(".minutes").html(0);
            $(".seconds").html(0);
        }
    }, 1000);
}

$(window).on("load", function() {
    // Handler for .load() called.
    $("#pageloader").fadeOut();
    masnry();

});


$(window).resize(function() {
    if ($(this).width() != width) {
        width = $(this).width();
        //console.log(width);
        animation();
    }
});

/*satyas js */

function set_background() {
    $('.set-bg').each(function() {
        if (typeof $(this).attr('data-mob-img') === 'undefined') {
            $(this).css({
                'background': 'url(' + $(this).attr('data-img') + ')',
                'background-size': 'cover'
            });
        } else {
            if (ww > mobile_breakbpoint) {
                $(this).css({
                    'background': 'url(' + $(this).attr('data-img') + ')',
                    'background-size': 'cover'
                });
            } else {
                $(this).css({
                    'background': 'url(' + $(this).attr('data-mob-img') + ')',
                    'background-size': 'cover'
                });
            }
        }
    });
}

$(document).ready(function() {

    $('.popup-youtube').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });
    set_background();


});