<?php
// error_reporting(E_ALL);
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
session_start();
$postData = $uploadedFile = $statusMsg = '';
$msgClass = 'errordiv';
$email = $_POST['email'];
$name = $_POST['name'];
// $subject = $_POST['subject'];
$number = $_POST['tel'];
$attachmentType = $_POST['attachmentType'];
//$message = $_POST['message'];
// Check whether submitted data is not empty
if(!empty($email) && !empty($name) && !empty($number)){		
	// Validate email
	if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
		$statusMsg = 'Please enter your valid email.';
	}else{
		$uploadStatus = 1;
		
		// Upload attachment file
		if(!empty($_FILES["file"]["name"])){
			
			// File path config
			$targetDir = "uploads/";
			$fileName = basename($_FILES["file"]["name"]);
			$targetFilePath = $targetDir . $fileName;
			$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
			
			// Allow certain file formats
			$allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg', 'mp4');
			if(in_array($fileType, $allowTypes)){
				if($_FILES["file"]["size"] < "31457280"){				
					// Upload file to the server
					if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
						$uploadedFile = $targetFilePath;
					}else{
						$uploadStatus = 0;
						$statusMsg = "Sorry, there was an error uploading your file.";
					}
				}else{
					$uploadStatus = 0;
					$statusMsg = 'Sorry, Max upload file size limit is 30 mb.';
				}
			}else{
				$uploadStatus = 0;
				$statusMsg = 'Sorry, only PDF, DOC, JPG, JPEG, PNG and mp4 files are allowed to upload.';
			}
		}
		
		if($uploadStatus == 1){
			
			// Recipient
			$toEmail = 'info@jashnepunjabi.org';

			// Sender
			$from = $email;
			$fromName = $name;
			
			// Subject
			$emailSubject = 'Entry for '.$attachmentType." Talent By ".$name.' from Jashn-e-punjabi Website';
			
			// Message 			
			$downloadPath = 'http://jashnepunjabi.com/download?fname='.$fileName.'&ext='.$fileType.'&type='.$_FILES["file"]["type"];
			$htmlContent = '
				<p><b>Name:</b> '.$name.'</p>
				<p><b>Email:</b> '.$email.'</p>                    
				<p><b>Number:</b>'.$number.'</p>
				<p><a target="_blank" href="'.$downloadPath.'">Click me to download the file</a></p>';
			//echo $htmlContent;die;
			// Header for sender info
			$headers = "From: $fromName"." <".$from.">";

			if(!empty($uploadedFile) && file_exists($uploadedFile)){
				
				require_once("class.phpmailer.inc.php");
				$mail = new PHPMailer();						
				$mail->From = "info@jashnepunjabi.org";
				$mail->FromName = "Jashnepunjab Admin";
				//$mail->AddAddress("info@jashnepunjabi.org");
				$mail->AddAddress("info@jashnepunjabi.org");
				
				$mail->Subject = $emailSubject;
				$mail->Body = $htmlContent;
				//$mail->AddAttachment($uploadedFile);
				$mail->ContentType = "text/html";
				$mail->Send();
				
				// Delete attachment file from the server
				//@unlink($uploadedFile);
			}else{
				 // Set content-type header for sending HTML email
				$headers .= "\r\n". "MIME-Version: 1.0";
				$headers .= "\r\n". "Content-type:text/html;charset=UTF-8";
				
				// Send email
				$mail = mail($toEmail, $emailSubject, $htmlContent, $headers); 
			}
			
			// If mail sent
			if($mail){
				$message = 'Your contact request has been submitted successfully !';
				$_SESSION['msgg'] = $message;
				//$postData = '';
				header("Location: http://jashnepunjabi.com");
			}else{
				$message = 'Your contact request submission failed, please try again.';
				$_SESSION['msgg'] = $message;
				header("Location: http://jashnepunjabi.com");
			}
		}
	}
}

?>
